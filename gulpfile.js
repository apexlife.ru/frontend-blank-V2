/*
 * 
 * Определяем переменные 
 *
 */

var gulp = require('gulp'), // Сообственно Gulp JS
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    csso = require('gulp-csso'), // Минификация CSS
    autoprefixer = require('gulp-autoprefixer'), // Auto Prefixer CSS
    sass = require('gulp-sass'),// Конверстация SASS (SCSS) в CSS
    csscomb = require('gulp-csscomb'), // Структуирование css очередностей
    notify = require('gulp-notify'), // Нотификации об зовершениях компиляций
    gutil = require('gulp-util'), // Требуется для работы FTP
    svgSprite = require('gulp-svg-sprites'), //Генератор SVG Спрайтов
    ftp = require('gulp-ftp');// Выгружаем проект на хостинг

/*
 * 
 * Создаем задачи (таски) 
 *
 */

// Задача "sass". Запускается командой "gulp sass"
gulp.task('sass', function () { 
	gulp.src('./assets/sass/style.scss') // файл, который обрабатываем
		.pipe(sass().on('error', sass.logError)) // конвертируем sass в css
        .pipe(autoprefixer({
            browsers: ['last 15 versions'],
            cascade: false
        }))
        .pipe(csscomb())
		.pipe(csso()) // минифицируем css, полученный на предыдущем шаге
		.pipe(gulp.dest('./assets/css/')) // результат пишем по указанному адресу
        .pipe(notify('CSS done!')); //GULP Notify
});

// Задача "js". Запускается командой "gulp js"
gulp.task('js', function() {
    gulp.src([
        './assets/js/libs/jquery.js',
        './assets/js/libs/webfont.js',
        './assets/js/script.js'
    ]) // файлы, которые обрабатываем
        .pipe(concat('min.js')) // склеиваем все JS
        .pipe(uglify()) // получившуюся "портянку" минифицируем
        .pipe(gulp.dest('./assets/js/')) // результат пишем по указанному адресу
        .pipe(notify('JavaScript Done!'))
});

// Задача "ftp". Запускается командой "gulp ftp"
gulp.task('ftp', function () {
    return gulp.src([
        '**/*',
        '*',
        'assets/**/*',
        'public/**/*',
        '!node_modules/**',
        '!bower_components/**',
        '!.gitignore',
        '!.gulpfile.js',
        '!.package.json'
    ])
        .pipe(ftp({
            host: '100.70.64.247',
            auth: 'kanta',
            pass: '1',
            remotePath: '/var/www/html/frontend-blankV2/'
        }))

        .pipe(gutil.noop());
});

// Задача "svg". Запускается командой "gulp svg"
gulp.task('svg', function () {
    return gulp.src('assets/img/svg/**/*.svg')
        .pipe(svgSprite())
        .pipe(gulp.dest("assets/svg/"));
});

// Задача "watch". Запускается командой "gulp watch"
// Она следит за изменениями файлов и автоматически запускает другие задачи
gulp.task('watch', function () {
	// При изменение файлов *.scss в папке "sass" и подпапках запускаем задачу sass
	gulp.watch('./assets/sass/**/*.scss', ['sass']);
	// При изменение файлов *.js папке "js" и подпапках запускаем задачу js
	gulp.watch('./assets/js/script.js', ['js']);
	// При изменение любых файлов в папке "img" и подпапках запускаем задачу img
	gulp.watch('./assets/img/**/*', ['images']);
});